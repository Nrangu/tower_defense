﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Test2
{

    public class Sell : MonoBehaviour
    {
        #region private fields
        [SerializeField] GameObject _parent;
        [SerializeField] GameObject _menuSellPrefab;
        GameObject _menuSell;
        #endregion
        #region private methods

        private void OnMouseDown()
        {
            if (_menuSell != null) return;
            _menuSell = Instantiate(_menuSellPrefab);
            _menuSell.transform.position = gameObject.transform.position;
            _menuSell.GetComponent<MenuTower>().Parent = _parent;
        }
        #endregion
    }
}
