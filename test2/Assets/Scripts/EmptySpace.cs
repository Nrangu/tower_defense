﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Test2
{

    public class EmptySpace : MonoBehaviour
    {
        #region private fields
        [Tooltip("Префаб меню постройки баше")]
        [SerializeField] GameObject _buyMenu;
        GameObject _menu = null;
        #endregion
        #region private methods
        private void OnMouseDown()
        {
            if (_menu != null) return;

            _menu = Instantiate(_buyMenu);
            float z = _menu.transform.position.z;
            _menu.transform.position = new Vector3( transform.position.x, transform.position.y,z);
            _menu.GetComponent<MenuTower>().Parent = gameObject;
        }
        #endregion


    }
}