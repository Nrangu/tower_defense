﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library {

    public enum GameState { START, PLAY, PAUSE, GAMEOVER };

     public class  Game
    {
        #region private fields
         GameState _state = GameState.START;

        #endregion

        #region public fields
        public Action start = ()=>{ };
        public Action gameOver = () => { };
        #endregion
        #region public methods, properties
        public GameState State
        {
            get
            {
                return _state;
            }

            set
            {
                _state = value;
                SetState(_state);
            }
        }
        #endregion
        #region private methods
        void SetState(GameState state_)
        {
            switch(state_)
            {
                case GameState.START:
                     start();
                    break;
                case GameState.GAMEOVER:
                    _state = GameState.PAUSE;
                    gameOver();
                    break;
            }
        }
        #endregion
    }
}
