﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Test2
{
    public class SellTower : MonoBehaviour
    {
        #region private fields
        [SerializeField] GameObject _parent;
        [SerializeField] GameObject _emptySpace;
        #endregion
        #region private methods
        // Start is called before the first frame update
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {

        }
        private void OnMouseDown()
        {
            Vector3 position = _parent.GetComponent<MenuTower>().Parent.transform.position;
            GameObject tmp = Instantiate(_emptySpace);
            position.z = tmp.transform.position.z;
            tmp.transform.position = position;
            Test2.Instace().Main.ChangeCoins( (int)_parent.GetComponent<MenuTower>().Parent.GetComponent<Tower>().Data.SellingPrice );
            _parent.GetComponent<MenuTower>().DestroyParent();
            
        }
        #endregion
    }
}