﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Test2
{
    public class Close : MonoBehaviour
    {
        #region private fields
        [Tooltip("Объект содержащий все элементы меню")]
        [SerializeField] GameObject _parent;
        #endregion
        #region private methods
        private void OnMouseDown()
        {
            Destroy(_parent);
        }
        #endregion
    }
}