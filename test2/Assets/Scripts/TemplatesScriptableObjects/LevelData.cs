﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
namespace Test2
{
    [CreateAssetMenu(menuName ="Test2/LevelData",fileName ="LevelData")]
    public class LevelData : ScriptableObject
    {
        #region private fields

        [Tooltip("Количесто жизни замка")]
        [SerializeField] float _castleHeath = 0;
        [Tooltip("Стартовое количество монет")]
        [SerializeField] int _coins = 0;
        [Tooltip("Башни доступные для данного уровня")]
        [SerializeField] TowerData[] _towers;
        [Tooltip("Массив вол индекс это номер волны, значение количество врагов")]
        [SerializeField] int[] _waves;
        [Tooltip("Враги для данного уровня")]
        [SerializeField] EnemyData[] _enemies;
        #endregion
        #region public properties, methods

        public int[] Waves
        {
            get
            {
                return _waves;
            }
        }
        public int GetCountEnemiesInWave(int wave_)
        {
            if (wave_ >= _waves.Length) return -1;
            return _waves[wave_];
        }
        public EnemyData[] Enemies
        {
            get
            {
                return _enemies;
            }
        }
        public EnemyData GetEnemy(int index_)
        {
            if (index_ >= _enemies.Length) return null;
            return _enemies[index_];
        }
        public float CastleHealth
        {
            get
            {
                return _castleHeath;
            }
        }
        public int Coins
        {
            get
            {
                return _coins;
            }
        }

        public TowerData[] Towers
        {
            get
            {
                return _towers;
            }
        }
        #endregion

    }
}