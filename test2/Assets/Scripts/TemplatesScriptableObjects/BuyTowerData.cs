﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Test2
{
    [CreateAssetMenu(menuName ="Test2/BuyTowerData", fileName ="BuyTowerData")]
    public class BuyTowerData : ScriptableObject
    {
        #region private fields
        [Tooltip("Изображение если покупка возможна")]
        [SerializeField] Sprite _sprite;

        [Tooltip("Изображение если покупка невозможна")]
        [SerializeField] Sprite _disableSprite;

        [Tooltip("Данные башни котурая будет куплена")]
        [SerializeField] TowerData _towerData;

        [Tooltip("Префаб(шаблон) башни")]
        [SerializeField] GameObject _towerPrefab;

        #endregion
        #region public properties
        public Sprite Sprite
        {
            get
            {
                return _sprite;
            }
        }

        public Sprite DisableSprite
        {
            get
            {
                return _disableSprite;
            }
        }

        public TowerData TowerData
        {
            get
            {
                return _towerData;
            }
        }
        public GameObject TowerPrefab
        {
            get
            {
                return _towerPrefab;
            }
        }
        #endregion

    }
}