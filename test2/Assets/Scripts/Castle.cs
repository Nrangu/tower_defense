﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Test2
{

    public class Castle : MonoBehaviour
    {
        #region private fields
        [SerializeField] CastleData _data;
        #endregion
        #region private methods
        private void Start()
        {
            if (_data != null)
            {
                Init(_data);
            }
        }
        private void OnTriggerEnter2D(Collider2D collision)
        {
            GameObject tmp = collision.gameObject;
            if (tmp.tag == "Enemy")
            {
                
                float damage = tmp.GetComponent<Enemy>().Kamikadze();
                Test2.Instace().Main.Damage(damage);
            }
        }
        #endregion
        #region public methods, properties
        public void Init( CastleData data_)
        {
            _data = data_;
            GetComponent<SpriteRenderer>().sprite = _data.Sprite;
        }
        public float Health
        {
            get
            {
                return _data.Health;
            }
        }
        #endregion
    }
}
