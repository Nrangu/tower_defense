﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Test2
{
    public class Tower : MonoBehaviour
    {
        #region private fields
        [SerializeField] TowerData _data;
        [SerializeField] GameObject _menuSell;
        List<GameObject> _enemies = new List<GameObject>();
        float _time;
        #endregion
        #region private methods
        // Start is called before the first frame update
        void Start()
        {
            if (_data != null)
            {
                Init(_data);
            }
        }

        // Update is called once per frame
        void Update()
        {
            if (!Test2.Instace().IsPlay) return;
            if (_enemies.Count > 0)
            {
                Shoot();
            }
        }
        private void Shoot()
        {
            if (_time >= _data.ShootInterval)
            {
                _time = 0;
                Enemy _enemy = GetEnemy();
                if (_enemy)
                {
                    _enemy.Damage(_data.Damage);
                }
            }
            _time += Time.deltaTime;
        }
        private Enemy GetEnemy()
        {
            Transform _castlePosition = Test2.Instace().Main.CastlePosition();
            GameObject enemy = _enemies[0];
            float dist = Vector2.Distance(_castlePosition.position, enemy.transform.position);
            foreach(GameObject obj in _enemies)
            {
                float tmpDist = Vector2.Distance(_castlePosition.position, obj.transform.position);
                if (tmpDist < dist)
                {
                    dist = tmpDist;
                    enemy = obj;
                }
            }
            return enemy.GetComponent<Enemy>();
        }
        private void OnTriggerEnter2D(Collider2D collision)
        {
            GameObject  tmp = collision.gameObject;
            if (tmp.tag == "Enemy")
            {
                _enemies.Add(tmp);
            }
        }
        private void OnTriggerExit2D(Collider2D collision)
        {
            GameObject tmp = collision.gameObject;
            if (tmp.tag == "Enemy")
            {
                _enemies.Remove(tmp);
            }
        }
        #endregion
        #region public methods, properties
        public void Init(TowerData data_)
        {
            _data = data_;
            GetComponent<SpriteRenderer>().sprite = _data.Sprite;
            GetComponent<CircleCollider2D>().radius = _data.Range;
            _time = _data.ShootInterval;
        }
        public TowerData Data
        {
            get
            {
                return _data;
            }
        }
        #endregion
    }
}
