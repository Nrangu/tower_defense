﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Test2
{

    public class Spawner : MonoBehaviour
    {
        #region private fields
        Transform[] _path;
        EnemyData[] _enemies;
        [SerializeField]GameObject _enemy;
        int[] _waves;
        int _currentWave = 0;
        int _countEnemies = 0;
        int _currentEnemy = 0;
        [SerializeField] float _interval = 2;
        float _time;
        #endregion
        #region private methods
        // Start is called before the first frame update
        void Start()
        {
            _time = _interval;
            _path = Test2.Instace().Main.Level.Path;
            _waves = Test2.Instace().Main.Level.Data.Waves;
            _enemies = Test2.Instace().Main.Level.Data.Enemies;
            Test2.Instace().Main.Waves = _waves.Length;
            Test2.Instace().Main.Wave = _currentWave + 1;
        }

        // Update is called once per frame
        void Update()
        {
            if (!Test2.Instace().IsPlay) return;
            if (_currentWave >= _waves.Length-1) return;
            if (_time >= _interval)
            {
                _time = 0;
                GameObject tmp = Instantiate(_enemy);
                Vector3 position = transform.position;
                position.z = tmp.transform.position.z;
                tmp.transform.position = position;
                tmp.GetComponent<Enemy>().Init(GetRandomEnemy(), _path);

                _currentEnemy++;
                if (_currentEnemy > _waves[_currentWave])
                {
                    _currentEnemy = 0;
                    _currentWave++;
                    if (_currentWave < _waves.Length)
                    {
                        Test2.Instace().Main.Wave = _currentWave + 1;
                    }
                }


            }

            _time+=Time.deltaTime;
        }
        EnemyData GetRandomEnemy()
        {

            return _enemies[Random.Range(0, _enemies.Length)];
        }
        #endregion
    }
}